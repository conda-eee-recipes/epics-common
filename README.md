epics-common conda recipe
=========================

Home: https://github.com/epics-base/epics-base

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: conda metapackage that includes common EPICS modules
